﻿using System;

namespace task13
{
    class Program
    {
        static void Main(string[] args)
        {
            // Magdeli Holmøy Asplin
            // 8/20/2019

            // This program creates three strings and three integers, tries to place them in two
            // chaosarrays and then it tries to retrieve an item from each array

            #region Attributes
            // The strings, integers and chaosarrays are declared and the variables are assigned values

            string magdeli = "Magdeli Holmøy Asplin";
            string city = "Bergen sure gets rainy!";
            string task = "These tasks usually need some heavy brainusage!";

            int ten = 10;
            int twelve = 12;
            int twentyfive = 25;

            ChaosArray<string> stringArray = new ChaosArray<string>();
            ChaosArray<int> intArray = new ChaosArray<int>();

            #endregion


            #region Adding to the ChaosArray
            // In this region the variables are added to the chaosarrays if possible

            try
            {
                stringArray.Add(magdeli);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                stringArray.Add(city);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                stringArray.Add(task);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                intArray.Add(ten);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                intArray.Add(twelve);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                intArray.Add(twentyfive);
            }
            catch (OccupiedPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            #endregion


            #region Retrieving an element of the ChaosArray
            // In this region two items are tried retrieved from the two chaosarrays

            Console.WriteLine("For the chaosarray of strings:");

            try
            {
                stringArray.Retrieve();
            }
            catch (EmptyPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("For the chaosarray of integers:");

            try
            {
                intArray.Retrieve();
            }
            catch (EmptyPlacementException ex)
            {
                Console.WriteLine(ex.Message);
            }


            #endregion
        }
    }
}
