﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13
{
    class ChaosArray<T>
    {
        // Magdeli Holmøy Asplin
        // 8/20/2019

        // This class creates an array of unspecified generic type that can add an item to a random
        // spot in the array, and retrieve an item from a random spot in the array

        #region Attributes
        // The variables and objects used are declared and some are assigned values

        private int arraySize;
        private int defaultSize = 10;
        private T[] chaosArray;
        Random arrayPlacement;
        int placement;

        #endregion


        #region Constructors
        // Two constructors are created, one empty that sets a default arraysize,
        // and one that takes in an integer as the arraysize

        public ChaosArray()
        {
            arraySize = defaultSize;
            chaosArray = new T[arraySize];
        }

        public ChaosArray(int arraySize)
        {
            this.arraySize = arraySize;
            chaosArray = new T[arraySize];

        }

        #endregion


        #region Methods
        // There are two methods in this class, one to add an item to a random place in the array,
        // and one to retrieve the item from a random place in the array

        public void Add(T value)
        {
            // Adding an item to the array. If the spot is occupied it throws the
            // OccupiedPlacementException

             arrayPlacement = new Random();
             placement = arrayPlacement.Next(arraySize);

            if(!EqualityComparer<T>.Default.Equals(chaosArray[placement], default(T)))
            {
                throw new OccupiedPlacementException($"The spot you tried to place the variable in was already occupied.");
            }
            else
            {
                chaosArray[placement] = value;
                Console.WriteLine($"Added {value} to placement {placement}.");
            }
        }

        public void Retrieve()
        {
            // Retrieves an item from the array. If the spot is empty it throws the
            // EmptyPlacementException

            arrayPlacement = new Random();
            placement = arrayPlacement.Next(arraySize);

            if (!EqualityComparer<T>.Default.Equals(chaosArray[placement], default(T)))
            {
                Console.WriteLine($"On placement {placement} in the chaosarray we find {chaosArray[placement]}.");
            }
            else
            {
                throw new EmptyPlacementException("The spot you tried to retrieve from was empty.");
            }

            #endregion
        }

    }
}
