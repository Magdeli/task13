﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13
{
    class EmptyPlacementException : Exception
    {
        // Magdeli Holmøy Asplin
        // 8/20/2019

        #region Constructors
        public EmptyPlacementException() { }

        public EmptyPlacementException(string message) : base(message)
        {

        }

        public EmptyPlacementException(string message, Exception inner) : base(message, inner)
        {

        }

        #endregion
    }
}
