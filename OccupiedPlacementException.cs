﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13
{
    class OccupiedPlacementException : Exception
    {
        // Magdeli Holmøy Asplin
        // 8/20/2019

        public OccupiedPlacementException() { }

        public OccupiedPlacementException(string message) : base(message)
        {

        }

        public OccupiedPlacementException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
